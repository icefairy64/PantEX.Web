const puppeteer = require('puppeteer');
const download = require('download');

require('./config').onConfigComplete(init, this);

/** @type {puppeteer.Browser} */
let browser;

let navigationTimeout = 30000;

let _bpResolve;
let _bpReject;

const browserPromise = new Promise((resolve, reject) => {
    _bpResolve = resolve;
    _bpReject = reject;
});

async function init(config) {
    if (config.puppeteer.navigationTimeout != null) {
        navigationTimeout = config.puppeteer.navigationTimeout;
    }
    console.log('Starting browser...');
        browser = await puppeteer.launch({
            executablePath: config && config.puppeteer && config.puppeteer.chromiumPath
        });
    _bpResolve(browser);
    console.log('Browser is started');
}

module.exports.fetchBuffer = async function (url) {
    await browserPromise;
    const page = await browser.newPage();
    let cookies;
    let requestHeaders;
    let mime;
    try {
        await page._client.send('Page.setDownloadBehavior', { behavior: 'allow', downloadPath: './storage' });
        const response = await page.goto(url, { timeout: navigationTimeout });
        const resources = {};
        /*page._client.on('Network.dataReceived', event => {
            const request = page._networkManager._requestIdToRequest.get(event.requestId);
            if (!request.url().startsWith('data:')) {
                const length = event.dataLength;
                if (request.url() in resources)
                resources[request.url()] += length;
                else
                resources[request.url()] = length;
            }
            request.response().buffer().then((buf) => {
                console.log(buf);
            });
            console.log(resources);
        });*/
        cookies = await page.cookies();
        mime = response.headers()['content-type'];
        const buffer = await response.buffer();
        const ok = response.ok();
        return {
            buffer: buffer,
            mimeType: mime,
            ok: ok
        };
    }
    catch (e) {
        //stream: download(url).on('request', console.log).on('response', console.log).on('downloadProgress', console.log),
        return {
            buffer: await download(url),
            mimeType: mime,
            ok: true
        };
    }
    finally {
        page.close();
    }
}

module.exports.fetchText = async function (url) {
    await browserPromise;
    const page = await browser.newPage();
    const response = await page.goto(url, { timeout: navigationTimeout, waitUntil: 'domcontentloaded' });
    const text = await response.text();
    page.close();
    return text;
}

module.exports.fetchUrl = async function (url) {
    await browserPromise;
    const page = await browser.newPage();
    const response = await page.goto(url, { timeout: navigationTimeout });
    response.free = () => {
        page.close();
    }
    return response;
}

/** @param {pageCallback} callback */
module.exports.executeOnPage = async function (url, callback) {
    await browserPromise;
    const page = await browser.newPage();
    try {
        await page.goto(url, { timeout: navigationTimeout });
        return await callback(page);
    }
    finally {
        page.close();
    }
}

/** 
 * @callback pageCallback
 * @param {puppeteer.Page} page
*/