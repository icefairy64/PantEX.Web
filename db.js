const mongo = require('mongodb');
const _ = require('lodash');
const _f = require('lodash/fp');

const config = require('./config');

/** @type {mongo.Db} */
let connection = null;

const listeners = [];

config.onConfigComplete(init, this);

function init(config) {
    console.log('Initializing DB...');
    mongo.MongoClient.connect(config.mongoUri, config.mongoOptions, (err, conn) => {
        if (err) {
            console.error(err);
        } else {
            console.log(`Connected to Mongo server at ${config.mongoUri}`);
            connection = conn.db(config.mongoDb);
            for (let listener of listeners) {
                listener.fn(listener.scope);
            }
        }
    });
}

function ifExists(value, fn) {
    if (value != null) {
        fn.call(value);
    }
}

function toId(value) {
    if (_.isString(value)) {
        return mongo.ObjectID.createFromHexString(value);
    }
    return value;
}

/** @returns {mongo.Cursor} */
function applyPaging(options, cursor) {
    if (options.offset) {
        cursor = cursor.skip(+options.offset);
    }
    cursor = cursor.limit(+(options.limit || 20));
    return cursor;
}

/** @returns {mongo.Cursor} */
function applySorting(options, cursor) {
    return cursor.sort(options);
}

module.exports.onInitComplete = function (listener, scope) {
    if (connection != null) {
        listener.call(scope);
    } else {
        listeners.push({ fn: listener, scope: scope });
    }
}

/** @returns {Promise<Array>} */
module.exports.posts = function (options) {
    const query = {};
    ifExists(options.tags, function () {
        query.tags = { $all: _.isArray(this) ? this : [this] }
    });
    ifExists(options.id, function () {
        query._id = toId(this);
    });
    if (options.withNonLocalAttachments) {
        // Matches attachments where all URLs are non-objects (strings)
        query.attachments = { $elemMatch: { $not: { $elemMatch: { urls: { $type: 3 } } } } };
    }

    const col = connection.collection('posts');

    const cursor = _f.compose(
        _.partial(applyPaging, options),
        _.partial(applySorting, { createdAt: -1 }),
        _.bind(col.find, col)
    )(query);

    return cursor.toArray();
}

/** @returns {Promise<mongo.Db>} */
module.exports.posts.new = function (post) {
    const col = connection.collection('posts');
    let promise = Promise.resolve(0);
    if (post.source != null && post.source != '') {
        promise = col.find({ source: post.source })
            .count();
    }
    return promise
        .then((count) => {
            if (count == 0) {
                return _f.compose(
                    _.bind(col.insertOne, col),
                    _.partialRight(_.defaults, {
                        tags: [],
                        attachments: [],
                        createdAt: _.now()
                    }),
                    _.cloneDeep
                )(post);
            } else {
                throw 'Post with provided source already exists';
            }
        });
}

/** @returns {Promise} */
module.exports.sourceOptions = function (sourceName) {
    const query = {};
    if (sourceName != null) {
        query.name = sourceName;
    }
    return connection.collection('sources').find(query).toArray();
}

/** @returns {Promise} */
module.exports.sourceOptions.update = function (sourceName, options) {
    return connection.collection('sources').updateOne({
        name: sourceName
    }, { $set: options }, { upsert: true });
}

module.exports.users = function (userName) {
    if (userName == null) {
        return Promise.resolve(null);
    }
    return connection.collection('users')
        .find({ name: userName })
        .toArray()
        .then((a) => a[0]);
}

module.exports.users.new = function (user) {
    return connection.collection('users')
        .insertOne(user);
}

module.exports.sessions = function (sessionId) {
    if (sessionId == null) {
        return Promise.resolve(null);
    }
    return connection.collection('sessions')
        .find({ _id: mongo.ObjectID.createFromHexString(sessionId) })
        .toArray()
        .then((arr) => {
            if (arr.length === 0) {
                return null;
            }
            const session = arr[0];
            if (session.expires_at <= _.now()) {
                connection.collection('sessions')
                    .remove({ _id: session._id });
                return null;
            }
            return session;
        });
}

module.exports.sessions.new = function (userName) {
    return connection.collection('sessions')
        .insertOne({
            user_name: userName,
            expires_at: _.now() + 24*60*60*1000
        });
}

/** @returns {Promise<Array>} */
module.exports.collections = function (userName, options) {
    if (userName == null) {
        return Promise.resolve(null);
    }
    return connection.collection(userName + '_collections')
        .find(options || {})
        .toArray();
}

/** @returns {Promise<Array>} */
module.exports.collections.forPost = function (userName, postId) {
    return module.exports.collections(userName, {
        posts: toId(postId)
    });
}

module.exports.collections.new = function (userName, config) {
    if (userName == null || config == null) {
        return Promise.reject('Invalid arguments');
    }
    return connection.collection(userName + '_collections')
        .insertOne(config)
        .then((result) => result.insertedId);
}

module.exports.collections.addItem = function (userName, collectionId, postId) {
    if (collectionId == null || postId == null) {
        return Promise.reject('Invalid arguments');
    }
    return connection.collection(userName + '_collections')
        .updateOne({ _id: toId(collectionId) }, { $push: { posts: toId(postId) } })
        .then((result) => {
            if (result.modifiedCount == 0) {
                return Promise.reject('No such collection');
            }
            return result;
        });
}

module.exports.collections.removeItem = function (userName, collectionId, postId) {
    if (collectionId == null || postId == null) {
        return Promise.reject('Invalid arguments');
    }
    return connection.collection(userName + '_collections')
        .updateOne({ _id: toId(collectionId) }, { $pull: { posts: toId(postId) } })
        .then((result) => {
            if (result.modifiedCount == 0) {
                return Promise.reject('No such collection');
            }
            return result;
        });
}

module.exports.posts.unlazyAttachment = function (postId, attachmentId, realUrl) {
    return connection.collection('posts')
        .updateOne({
            _id: toId(postId), 
            attachments: { 
                $elemMatch: { 
                    '_id': toId(attachmentId) 
                }
            } 
        }, {
            $pull: {
                'attachments.$.tags': 'lazy'
            },
            $push: {
                'attachments.$.urls': { $position: 0, $each: [ realUrl ] }
            }
        });
},

module.exports.posts.addAttachmentUrl = function (postId, attachmentIndex, url) {
    const attachementUrlsSelector = 'attachments.' + attachmentIndex + '.urls';
    const push = { };
    push[attachementUrlsSelector] = { $position: 0, $each: [ url ] };
    return connection.collection('posts')
        .updateOne({
            _id: toId(postId)
        }, {
            $push: push
        });
}