const fs = require('fs');

const express = require('express');
const router = express.Router();
const request = require('request');
const _ = require('lodash');

const db = require('../db');
const crawler = require('../crawler');
const localizer = require('../localizer');

let storageRoot;

require('../config').onConfigComplete(init, this);

function init(config) {
    storageRoot = config.storage.path;
}

router.use(express.json());

router.route('/posts')
    .get((req, res) => {
        const options = req.query;
        if (options.tags) {
            options.tags = options.tags.split(/\s/g);
        }
        db.posts(options)
            .then((p) => {
                res.send(JSON.stringify(p));
            });
    })
    .post((req, res) => {

    });

router.route('/posts/:id/collections')
    .get((req, res) => {
        if (req.userName == null) {
            res.send(500, {
                error: 'Not logged in'
            });
        } else {
            db.collections.forPost(req.userName, req.params.id)
                .then((cols) => res.send(200, { collections: cols }))
                .catch((e) => res.send(500, { error: e }));
        }
    });

router.route('/sources')
    .get((req, res) => {
        db.sourceOptions(req.query.name)
            .then((p) => {
                res.send(JSON.stringify(p));
            });
    })
    .post((req, res) => {
        db.sourceOptions.update(req.body.name, req.body)
            .then((p) => {
                res.send("Success");
            })
            .catch((r) => {
                res.send(500, JSON.stringify(r));
            })
    });

router.route('/placebo')
    .get((req, res) => {
        res.send({ user_name: req.userName })
    });

router.route('/collections')
    .get((req, res) => {
        if (req.userName == null) {
            res.send(500, {
                error: 'Not logged in'
            });
        } else {
            db.collections(req.userName)
                .then((collections) => res.send(200, { collections: collections }));
        }
    })
    .post((req, res) => {
        if (req.userName == null) {
            res.send(500, {
                error: 'Not logged in'
            });
        } else {
            db.collections.new(req.userName, req.body)
                .then((collectionId) => res.send(200, { collection_id: collectionId }))
                .catch((e) => res.send(500, { error: e }));
        }
    });

router.route('/collections/:id')
    .get((req, res) => {
        res.send(500, {
            error: 'Not implemented'
        });
    })
    .post((req, res) => {
        if (req.userName == null) {
            res.send(500, {
                error: 'Not logged in'
            });
        } else {
            db.collections.addItem(req.userName, req.params.id, req.body.post_id)
                .then(() => res.send(200))
                .catch((e) => res.send(500, { error: e }));
        }
    })
    .delete((req, res) => {
        if (req.userName == null) {
            res.send(500, {
                error: 'Not logged in'
            });
        } else {
            db.collections.removeItem(req.userName, req.params.id, req.body.post_id)
                .then(() => res.send(200))
                .catch((e) => res.send(500, { error: e }));
        }
    });

function readLocalFile(path) {
    return new Promise((resolve, reject) => {
        fs.readFile(path, (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
}

async function loadAttachment(attachment) {
    const headers = attachment.headers || {};
    headers['User-Agent'] = 'PantEX/0.1';

    const localUrl = attachment.urls.find((url) => !_.isString(url) && url.local === true);

    if (localUrl != null) {
        return {
            attachment: attachment,
            url: localUrl.path,
            buffer: {
                buffer: await readLocalFile(storageRoot + '/' + localUrl.path),
                mimeType: localUrl.mimeType,
                ok: true
            },
            local: true
        }
    }

    if (attachment.tags.includes('lazy')) {
        let config = await require('../refiller/' + attachment.lazyResolveModule)(attachment.urls[0]);
        if (_.isString(config)) {
            config = {
                url: config
            }
        }
        if (config.headers) {
            for (let prop in config.headers) {
                headers[prop] = config.headers[prop];
            }
        }
        const buffer = await crawler.fetchBuffer(config.url);
        return {
            attachment: attachment,
            url: config.url,
            newUrl: config.url,
            buffer: buffer
        };
    } else {
        const buffer = await crawler.fetchBuffer(attachment.urls[0]);
        return {
            attachment: attachment,
            url: attachment.urls[0],
            buffer: buffer
        };
    }
}

router.route('/attachments/:postId/:attachmentId')
    .get((req, res) => {
        db.posts({ id: req.params.postId })
            .then((posts) => {
                if (posts.length > 0 && posts[0].attachments.length > req.params.attachmentId) {
                    return { post: posts[0], attachment: posts[0].attachments[req.params.attachmentId] };
                } else {
                    return Promise.reject('No such post or attachment exists');
                }
            })
            .then(async (result) => { return { post: result.post, attachment: await loadAttachment(result.attachment) } })
            .then((result) => {
                const attachment = result.attachment;
                const post = result.post;
                if (attachment.newUrl != null) {
                    //db.posts.unlazyAttachment(req.params.postId, attachment.attachment._id, attachment.newUrl);
                }
                if (!attachment.buffer.ok) {
                    throw new Error("Loading failed; result: " + JSON.stringify(attachment));
                }
                if (!attachment.local) {
                    localizer.saveAttachment(post, attachment.attachment, attachment.url, attachment.buffer.buffer, attachment.buffer.mimeType);
                }
                try {
                    console.log('Piping ' + attachment.url);
                    res.contentType(attachment.buffer.mimeType).send(attachment.buffer.buffer);
                }
                catch (e) {
                    res.status(500).send({ error: e });
                }
            })
            .catch((e) => {
                res.send(500, { error: e.message || e });
            });
    });

module.exports = router;