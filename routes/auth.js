const express = require('express');
const router = express.Router();

const db = require('../db');

router.use(express.json());

router.route('/')
    .get((req, res) => {
        const sessionId = req.cookies && req.cookies['session_id'];
        db.sessions(sessionId)
            .then((session) => {
                if (session == null) {
                    res.send(500, {
                        logged_in: false
                    });
                } else {
                    res.send(200, {
                        logged_in: true,
                        user_name: session.user_name
                    });
                }
            })
    })
    .post((req, res) => {
        db.users(req.body['user_name'])
            .then((user) => {
                if (user == null || user.password != req.body.password) {
                    res.send(500, {
                        logged_in: false
                    });
                } else {
                    db.sessions.new(req.body['user_name'])
                        .then((result) => {
                            if (result.insertedId == null) {
                                res.send(500, {
                                    logged_in: false
                                });
                            } else {
                                res.cookie('session_id', result.insertedId.toString(), { maxAge: 1000*60*60*24*30 })
                                    .send(200, {
                                        logged_in: true
                                    });
                            }
                        });
                }
            })
    });

module.exports = router;