let toastContainer = null;

function showError(error) {
    if (error.__proto__ === Blob.prototype) {
        const reader = new FileReader();
        reader.addEventListener('loadend', () => showError(reader.result));
        reader.readAsText(error);
        return;
    }

    const toastEl = document.createElement('div');
    toastEl.classList.add('toast');

    const textEl = document.createElement('span');
    textEl.textContent = 'Error: ' + (typeof error === 'string' ? error : JSON.stringify(error));

    toastEl.appendChild(textEl);
    toastContainer.appendChild(toastEl);

    window.setTimeout(() => toastContainer.removeChild(toastEl), 5000);
}

function init() {
    toastContainer = document.createElement('div');
    toastContainer.classList.add('toast-container');
    document.body.appendChild(toastContainer);
}

window.addEventListener('load', init);