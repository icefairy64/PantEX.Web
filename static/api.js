{
    function xhr(options) {
        const req = new XMLHttpRequest();
        if (options.responseType) {
            req.responseType = options.responseType;
        }
        if (typeof options.data === 'object') {
            options.data = JSON.stringify(options.data);
        }
        return new Promise((resolve, reject) => {
            req.open(options.method || 'GET', options.url);
            req.onload = function (ev) {
                if (this.status >= 200 && this.status < 300) {
                    resolve(req.response);
                } else {
                    reject(req.response);
                }
            };
            req.onerror = function () {
                reject(req.response);
            };
            for (let headerKey in options.headers || {}) {
                req.setRequestHeader(headerKey, options.headers[headerKey]);
            };
            req.send(options.data);
        });
    }

    function hex(buffer) {
        var hexCodes = [];
        var view = new DataView(buffer);
        for (var i = 0; i < view.byteLength; i += 4) {
          // Using getUint32 reduces the number of iterations needed (we process 4 bytes each time)
          var value = view.getUint32(i)
          // toString(16) will give the hex representation of the number without padding
          var stringValue = value.toString(16)
          // We use concatenation and slice for padding
          var padding = '00000000'
          var paddedValue = (padding + stringValue).slice(-padding.length)
          hexCodes.push(paddedValue);
        }
      
        // Join all the hex strings into one
        return hexCodes.join("");
      }

    window.PantEX = {};

    PantEX = window.PantEX;

    PantEX.posts = {};
    
    PantEX.posts.fetch = function (offset, limit) {
        return xhr({ url: '/api/posts?offset=' + (offset || 0) + '&limit=' + (limit || 20) })
            .then(JSON.parse);
    }

    PantEX.auth = {};

    PantEX.auth.login = function (userName, password) {
        var buffer = new TextEncoder("utf-8").encode(password);
        return window.crypto.subtle.digest('SHA-512', buffer)
            .then((pass) => {
                return xhr({ url: '/auth', method: 'POST', data: JSON.stringify({
                    user_name: userName,
                    password: hex(pass)
                }), headers: { 'Content-Type': 'application/json' }});
            });
    }

    PantEX.collections = {};

    PantEX.collections.fetch = function () {
        return xhr({ url: '/api/collections' })
            .then(JSON.parse)
            .then((res) => res.collections);
    }

    PantEX.collections.new = function (name) {
        return xhr({ url: '/api/collections', method: 'POST', data: {
            name: name
        }, headers: { 'Content-Type': 'application/json' } });
    }

    PantEX.collections.forPost = function (postId) {
        return xhr({ url: '/api/posts/' + postId + '/collections' });
    }

    PantEX.collections.addPost = function (collectionId, postId) {
        return xhr({ url: '/api/collections/' + collectionId, method: 'POST', data: {
            post_id: postId
        }, headers: { 'Content-Type': 'application/json' } });
    }

    PantEX.collections.removePost = function (collectionId, postId) {
        return xhr({ url: '/api/collections/' + collectionId, method: 'DELETE', data: {
            post_id: postId
        }, headers: { 'Content-Type': 'application/json' } });
    }
}