function login() {
    PantEX.auth.login(document.getElementById('login').value, document.getElementById('password').value)
        .then(() => { window.location = '/index.html' })
        .catch(showError);
}