thumbs = [];
collections = [];

currentCollection = null;

function fetchPage(offset) {
    return PantEX.posts.fetch(offset)
        .then((posts) => {
            for (let post of posts) {
                const thumb = createThumb(post);
                checkCollections(thumb);
                document.getElementById('container').appendChild(thumb.el);
                thumbs.push(thumb);
                thumb.el.addEventListener('click', (ev) => {
                    toggleCollectionInclusion(thumb);
                });
                thumb.el.addEventListener('contextmenu', (ev) => {
                    ev.preventDefault();
                    ev.stopPropagation();
                    loadDetail(post);
                });
            }
        });
}

function checkCollections(thumb) {
    const post = thumb.post;
    post.collections = collections.filter((col) => col.posts == null ? false : (col.posts.indexOf(post._id) >= 0)).map((col) => col._id);
    for (let collectionId of post.collections) {
        addCollection(thumb, collectionId);
    }
}

function addCollection(thumb, collectionId) {
    if (thumb.post.collections.findIndex((col) => col == collectionId) < 0) {
        thumb.post.collections.push(collectionId);
    }
    thumb.frameEl.classList.add('in-' + collectionId);
}

function removeCollection(thumb, collectionId) {
    const index = thumb.post.collections.findIndex((col) => col == collectionId);
    if (index >= 0) {
        thumb.post.collections.splice(index, 1);
    }
    thumb.frameEl.classList.remove('in-' + collectionId);
}

function init() {
    fetchPage()
        .then(() => {
            console.log('Adding scroll listener');
            window.onscroll = handleScrollEnd;
        });

    PantEX.collections.fetch()
        .then((result) => collections = result)
        .then(handleCollectionsLoad);
}

function createCollectionStyles(collection) {
    const sheet = document.styleSheets[0];
    const inactiveRule = sheet.cssRules[0];
    const activeRule = sheet.cssRules[1];
    const staticRule = '{ border: ' + inactiveRule.style.border + '; }';
    const index = sheet.insertRule('.thumb .thumb-frame.in-' + collection._id + staticRule, 2);
    collection.styleRule = sheet.cssRules[index];
    collection.setActive = function (value) {
        if (value) {
            this.styleRule.style.cssText = activeRule.style.cssText;
        } else {
            this.styleRule.style.cssText = inactiveRule.style.cssText;
        }
    }
}

function handleCollectionsLoad() {
    console.log('Collections loaded');
    const listEl = document.getElementById('collection-list');
    for (let collection of collections) {
        const itemEl = document.createElement('option');
        itemEl.text = collection.name;
        listEl.appendChild(itemEl);
        createCollectionStyles(collection);
    }
    for (let thumb of thumbs) {
        checkCollections(thumb);
    }
}

function loadDetail(post) {
    const container = document.getElementById('detail-container');
    container.post = post;
    container.style.display = 'flex';

    const img = document.createElement('div');
    img.classList.add('detail-image');
    // FIXME: remove hardcoded values
    img.style.backgroundImage = 'url("/api/attachments/' + post._id + '/' + Math.max(post.attachments.length - 1, 0) + '")';
    container.imageEl = img;
    container.free = function () {
        this.removeChild(this.imageEl);
    }

    xhr({ url: '/api/attachments/' + post._id + '/0', responseType: 'blob' })
        .then((body) => {
            container.imageUrl = URL.createObjectURL(body);
            container.free = function () {
                URL.revokeObjectURL(this.imageUrl);
                this.removeChild(this.imageEl);
            }
            container.imageEl.style.backgroundImage = 'url("' + container.imageUrl + '")';
        })
        .catch((e) => showError(e));

    img.addEventListener('click', handleDetailClick);
    container.appendChild(img);
    container.classList.add('visible');

    const thumbContainer = document.getElementById('container');
    thumbContainer.classList.add('blurred');
}

function handleDetailClick() {
    const container = document.getElementById('detail-container');
    if (container.free) {
        container.free();
    } else {
        console.error('Could not free container resources');
    }
    container.classList.remove('visible');

    const thumbContainer = document.getElementById('container');
    thumbContainer.classList.remove('blurred');
}

function handleScrollEnd(ev) {
    if (window.scrollY >= document.body.offsetHeight - document.body.clientHeight) {
        if (!window.fetching) {
            window.fetching = true;
            fetchPage(window.thumbs.length)
                .then(() => {
                    window.fetching = false;
                });
        }
    }
}

function toggleToolbox() {
    const toolbox = document.getElementById('toolbox');
    if (toolbox.shown) {
        toolbox.style.left = '-300px';
        toolbox.shown = false;
    } else {
        toolbox.style.left = '0px';
        toolbox.shown = true;
    }
}

function handleCollectionChange(ev) {
    if (currentCollection != null) {
        currentCollection.setActive(false);
    }
    currentCollection = collections[ev.target.selectedIndex];
    currentCollection.setActive(true);
}

function addPostToCollection(thumb, collectionId, callback) {
    thumb.frameEl.classList.add('pending');
    PantEX.collections.addPost(currentCollection._id, thumb.post._id)
        .then(() => {
            if (callback) {
                callback(undefined);
            }
            thumb.frameEl.classList.remove('pending');
            collections.find((c) => c._id === collectionId).posts.push(thumb.post._id);
        })
        .catch((e) => {
            showError(e);
            if (callback) {
                callback(e);
            }
            thumb.frameEl.classList.remove('pending');
        });
}

function removePostFromCollection(thumb, collectionId, callback) {
    thumb.frameEl.classList.add('pending');
    PantEX.collections.removePost(currentCollection._id, thumb.post._id)
        .then(() => {
            if (callback) {
                callback(undefined);
            }
            thumb.frameEl.classList.remove('pending');
            delete collections.find((c) => c._id === collectionId).posts[thumb.post._id];
        })
        .catch((e) => {
            showError(e);
            if (callback) {
                callback(e);
            }
            thumb.frameEl.classList.remove('pending');
        });
}

function toggleCollectionInclusion(thumb) {
    if (currentCollection == null) {
        return;
    }
    if (thumb.post.collections.includes(currentCollection._id)) {
        removeCollection(thumb, currentCollection._id);
        removePostFromCollection(thumb, currentCollection._id);
    } else {
        addCollection(thumb, currentCollection._id);
        addPostToCollection(thumb, currentCollection._id);
    }
}

window.onload = init;