function getThumbUrl(post) {
    const attachmentIndex = post.attachments.findIndex((a) => a.tags.includes('thumbnail'));
    return '/api/attachments/' + post._id + '/' + attachmentIndex;
}

function createThumb(post) {
    const obj = { post: post };
    obj.origWidth = post.attachments[0].width;
    obj.origHeight = post.attachments[0].height;

    const ratio = obj.origWidth / obj.origHeight;

    const el = document.createElement('div');
    el.classList.add('thumb');
    el.style.flexBasis = 20*ratio + '%';
    el.style.flexGrow = ratio;

    const frameEl = document.createElement('div');
    frameEl.classList.add('thumb-frame');

    const imgEl = document.createElement('div');
    imgEl.classList.add('thumb-img');
    imgEl.style.backgroundImage = 'url("' + getThumbUrl(post) + '")';
    imgEl.style.paddingBottom = 100/ratio + '%';

    el.appendChild(imgEl);
    el.appendChild(frameEl);

    obj.el = el;

    obj.updateHeight = function () {
        //this.el.style.height = this.el.clientWidth/ratio + 'px';
    };

    obj.imgEl = imgEl;
    obj.frameEl = frameEl;

    return obj;
}