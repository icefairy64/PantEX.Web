const http = require('http');
const _ = require('lodash');

const REQUEST_PERIOD = 200;

let nextRequestTimestamp = 0;

module.exports.request = function () {
    const now = _.now();
    const diff = Math.max(nextRequestTimestamp - now, 0);
    nextRequestTimestamp = now + REQUEST_PERIOD;
    if (diff == 0) {
        return http.request.call(http.request, arguments);
    } else {

    }
}