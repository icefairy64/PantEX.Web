const request = require('request');
const rp = require('request-promise-native');

const THUMB_PATTERN = /span class="thumb[\s\S]+?a href="(.+?)"[\s\S]+?img class=.*?preview[\s\S]*?src="(.+?)"[\s\S]+?title="(.+?)".+?width=(\d+?)\s.*?height=(\d+)/g;
const TAG_PATTERN = /a href="\/?tags=(.+?)"/;

const SANKAKU_PAGE_TEMPLATE = "https://{domain}.sankakucomplex.com/?page={page}";

const pageIter = require('./simple_page_iterator');

function convertPost(model) {
    const images = [];
    images.push({
        urls: [model.fullPageUrl],
        tags: ['source', 'lazy'],
        lazyResolveModule: 'sankaku_resolver',
        headers: {
            'Referer': 'https://' + model.domain + '.sankakucomplex.com/'
        }
    });
    images.push({
        urls: [model.thumbUrl],
        tags: 'thumbnail',
        width: model.thumbWidth,
        height: model.thumbHeight
    });
    const post = {
        attachments: images,
        source: model.fullPageUrl,
        sankaku_id: model.domain + '_' + model.fullPageUrl.substring(model.fullPageUrl.lastIndexOf('/') + 1),
        tags: model.tags
    };
    forMetaTag('Size', (res) => {
        const dimensions = res.value.split('x');
        post.attachments[0].width = +dimensions[0];
        post.attachments[0].height = +dimensions[1];
        delete post.tags[res.index];
    }, post);
    forMetaTag('Score', (res) => {
        delete post.tags[res.index];
    }, post);
    forMetaTag('User', (res) => {
        delete post.tags[res.index];
    }, post);
    forMetaTag('Rating', (res) => {
        post.rating = res.value.toLowerCase()[0];
        delete post.tags[res.index];
    }, post);
    post.tags = post.tags.filter((t) => t !== undefined);
    return post;
}

function forMetaTag(tag, handler, post) {
    const tagString = post.tags.find((t) => t != null && t.includes(tag + ':'));
    if (tagString != null) {
        handler({
            tagString: tagString,
            value: tagString.substring(tag.length + 1),
            index: post.tags.indexOf(tagString)
        });
    }
}

function fetchPage(domain, page) {
    return rp({
            url: SANKAKU_PAGE_TEMPLATE.replace('{domain}', domain).replace('{page}', page),
            headers: {
                'User-Agent': 'PantEX/0.1'
            }
        })
        .then((html) => {
            let result;
            const models = [];
            while (result = THUMB_PATTERN.exec(html)) {
                models.push({
                    fullPageUrl: 'https://' + domain + '.sankakucomplex.com' + result[1],
                    thumbUrl: 'https:' + result[2],
                    tags: result[3].split(' '),
                    thumbWidth: +result[4],
                    thumbHeight: +result[5],
                    domain: domain
                });
            }
            return models.map(convertPost);
        });
}

module.exports = function (options, domain) {
    return pageIter(options, {
        title: 'Sankaku',
        idField: 'sankaku_id',
        storageIdField: 'last_known_id',
        fetcher: function (page) { return fetchPage(domain, page); }
    });
}