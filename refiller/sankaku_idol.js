const sankaku = require('./sankaku');

module.exports.name = 'Sankaku Idol';
module.exports.inUse = false;

module.exports.refill = function (options) { return sankaku(options, 'idol');}