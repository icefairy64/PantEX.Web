const http = require('https');

const GELBOORU_POST_LIST_URI_PATTERN = 'https://gelbooru.com/index.php?page=dapi&s=post&q=index&json=1&pid={page}';
const GELBOORU_POST_PATTERN = 'https://gelbooru.com/index.php?page=post&s=view&id={id}';

function convertPost(model) {
    const images = [];
    images.push({
        urls: [model.file_url],
        width: model.width,
        height: model.height,
        tags: 'source_img'
    });
    if (model.sample) {
        let sampleUrl = model.file_url
            .replace('images/' + model.directory + '/', 'samples/' + model.directory + '/sample_');
        sampleUrl = sampleUrl.substring(0, sampleUrl.lastIndexOf('.')) + '.jpg';
        images.push({
            urls: [sampleUrl],
            width: model.sample_width,
            height: model.sample_height,
            tags: 'sample'
        });
    }
    let thumbUrl = model.file_url
            .replace('images/' + model.directory + '/', 'thumbnails/' + model.directory + '/thumbnail_');
    thumbUrl = thumbUrl.substring(0, thumbUrl.lastIndexOf('.')) + '.jpg';
    images.push({
        urls: [thumbUrl],
        tags: 'thumbnail'
    });
    return {
        tags: model.tags.split(' '),
        source: GELBOORU_POST_PATTERN.replace('{id}', model.id),
        gelbooru_id: model.id,
        hash: model.hash,
        rating: model.rating,
        attachments: images
    };
}

/** @returns {Promise<Array>} */
function fetchPage(page) {
    return new Promise((resolve, reject) => {
        http.request(GELBOORU_POST_LIST_URI_PATTERN.replace('{page}', page), (res) => {
            let buf = Buffer.alloc(0);
            res.on('data', (data) => {
                buf = Buffer.concat([buf, data]);
            })
            .on('end', () => {
                resolve(JSON.parse(buf.toString('utf8')));
            })
            .on('error', (e) => {
                reject(e);
            });
        }).end();
    });
}

module.exports.name = 'Gelbooru';
module.exports.inUse = false;

module.exports.refill = function (options) {
    return new Promise((resolve, reject) => {
        const f = function (page, allPosts) {
            console.log('Gelbooru: Fetching page ' + page);
            fetchPage(page)
                .then((models) => {
                    const posts = models.map(convertPost);
                    let lastKnownId = options.last_known_id;
                    let endReached = false;
                    for (let post of posts) {
                        if (post.gelbooru_id <= lastKnownId || lastKnownId == null) {
                            lastKnownId = allPosts.length == 0 ? posts[0].gelbooru_id : allPosts[0].gelbooru_id;
                            endReached = true;
                            break;
                        }
                    }
                    allPosts = allPosts.concat(posts.filter((p) => options.last_known_id == null || p.gelbooru_id > options.last_known_id));
                    if (endReached) {
                        resolve({
                            options: { last_known_id: lastKnownId },
                            posts: allPosts
                        });
                    } else {
                        f(page + 1, allPosts);
                    }
                })
                .catch((e) => reject(e));
        }
        f(0, []);
    });
}