const rp = require('request-promise-native');

const crawler = require('../crawler');

const SANKAKU_FULL_IMAGE_URL_PATTERN = /a href="(.+?)"[^<^>]+?contentUrl.+?>/;

function findAttachmentUrl(html) {
    const result = SANKAKU_FULL_IMAGE_URL_PATTERN.exec(html);
    return 'https:' + result[1].replace(/&amp;/g, '&');
}

module.exports = async function (pageUrl) {
    console.log('Sankaku: resolving attachment for post ' + pageUrl);
    const pageContent = await crawler.fetchText(pageUrl);
    const attachmentUrl = findAttachmentUrl(pageContent);
    return attachmentUrl;
}