const crawler = require('../crawler');
const pageIter = require('./simple_page_iterator');

const puppeteer = require('puppeteer');

function makePageUrl(pageIndex) {
    return `https://danbooru.donmai.us/posts?page=${pageIndex}`;
}

function convertPost(model) {
    const images = [];

    images.push({
        urls: [model['data-file-url']],
        width: model['data-width'],
        height: model['data-height'],
        tags: 'source_img'
    });

    if (model['data-file-url'] !== model['data-large-file-url']) {
        images.push({
            urls: [model['data-large-file-url']],
            tags: 'sample'
        });
    }
    
    images.push({
        urls: [model['data-preview-file-url']],
        tags: 'thumbnail'
    });

    const post = {
        attachments: images,
        source: 'https://danbooru.donmai.us/posts/' + model['data-id'],
        danbooru_id: +model['data-id'],
        hash: model['data-md5'],
        rating: model['data-rating'],
        tags: model['data-tags'].split(' ')
    };

    return post;
}

/** @param {puppeteer.Page} page */
async function processPage(page) {
    function getPosts() {
        const articles = document.getElementsByTagName('article');
        return [...articles]
            .map((a) => a.attributes)
            .map((attrs) => {
                const result = {};
                for (let attr of attrs) {
                    result[attr.name] = attr.value;
                }
                return result;
            });
    }

    return (await page.evaluate(getPosts))
        .map(convertPost);
}

function fetchPage(pageIndex) {
    return crawler.executeOnPage(makePageUrl(pageIndex), processPage);
}

module.exports.name = 'Danbooru';
module.exports.inUse = false;

module.exports.refill = function (options) {
    return pageIter(options, {
        title: 'Danbooru',
        idField: 'danbooru_id',
        storageIdField: 'last_known_id',
        fetcher: fetchPage
    });
}