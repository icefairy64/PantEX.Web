const config = require('../config');
const db = require('../db');

const providers = [];
let options = null;

config.onConfigComplete(init, this);

function init(config) {
    options = config.refiller;
    for (let providerName of options.providers) {
        const provider = require('./' + providerName);
        providers.push(provider);
    }
    db.onInitComplete(start, this);
}

function start() {
    for (let provider of providers) {
        setInterval(() => refill(provider), options.timeout || (5 * 60 * 1000));
        refill(provider);
    }
}

function refill(provider) {
    if (provider.inUse) {
        return;
    }
    provider.inUse = true;
    db.sourceOptions(provider.name)
        .then(opt => {
            console.log('Refilling from ' + provider.name);
            return provider.refill(opt[0] || {});
        })
        .then(result => {
            console.log('Done refilling from ' + provider.name + '; new posts: ' + result.posts.length);
            provider.inUse = false;
            db.sourceOptions.update(provider.name, result.options);
            for (let post of result.posts) {
                db.posts.new(post)
                    .catch((e) => console.error('Error while saving: ' + JSON.stringify(e) + '; post: ' + JSON.stringify(post)));
            }
        })
        .catch(e => console.error(e.stack));
}