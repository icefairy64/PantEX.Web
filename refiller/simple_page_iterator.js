module.exports = function (options, iterOptions) {
    return new Promise((resolve, reject) => {
        const f = function (page, allPosts) {
            console.log(iterOptions.title + ': Fetching page ' + page);
            iterOptions.fetcher(page)
                .then((models) => {
                    const posts = models;
                    let lastKnownId = options[iterOptions.storageIdField];
                    let endReached = false;
                    for (let post of posts) {
                        if (post[iterOptions.idField] <= lastKnownId || lastKnownId == null) {
                            lastKnownId = allPosts.length == 0 ? posts[0][iterOptions.idField] : allPosts[0][iterOptions.idField];
                            endReached = true;
                            break;
                        }
                    }
                    allPosts = allPosts.concat(posts.filter((p) => options[iterOptions.storageIdField] == null || p[iterOptions.idField] > options[iterOptions.storageIdField]));
                    if (endReached) {
                        const newOptions = {};
                        newOptions[iterOptions.storageIdField] = lastKnownId;
                        resolve({
                            options: newOptions,
                            posts: allPosts
                        });
                    } else {
                        f(page + 1, allPosts);
                    }
                })
                .catch((e) => reject(e));
        }
        f(0, []);
    });
}