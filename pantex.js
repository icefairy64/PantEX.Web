const http = require('http');
const mongo = require('mongodb');
const express = require('express');

const config = require('./config');
const db = require('./db');
const refill = require('./refiller/refill');
require('./crawler');
require('./localizer');

const app = express();

config.onConfigComplete(start, this);
config.loadConfig();

function start(config) {
    process.on('SIGTERM', () => {
        console.log('Received SIGTERM');
        process.exit(0);
    });

    app.use(express.static('./static'));

    app.use(require('cookie-parser')());
    app.use(require('./auth_middleware'));
    
    app.use('/api', require('./routes/api'));
    app.use('/auth', require('./routes/auth'));
    
    app.listen(config.port, '0.0.0.0');
    console.log('PantEX.Web started on port ' + config.port);
}