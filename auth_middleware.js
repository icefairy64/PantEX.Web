const db = require('./db');

module.exports = function (req, res, next) {
    const sessionId = req.cookies['session_id'];
    if (sessionId != null) {
        db.sessions(sessionId)
            .then((session) => {
                if (session != null) {
                    req.userName = session.user_name;
                }
                next();
            })
    } else {
        next();
    }
}