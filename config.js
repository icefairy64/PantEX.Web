const fs = require('fs');

const configLoadListeners = [];
let isConfigLoaded = false;
let loadedConfig = null;

function loadConfig() {
    fs.readFile('config.json', (err, data) => {
        if (err) {
            console.error(err);
        } else {
            const config = JSON.parse(data);
            loadedConfig = config;
            isConfigLoaded = true;
            configLoadListeners.forEach(f => f.fn.call(f.scope || this, config));
        }
    });
}

module.exports.onConfigComplete = function (handler, scope) {
    if (!isConfigLoaded) {
        configLoadListeners.push({ fn: handler, scope: scope });
    } else {
        handler.call(scope || this, loadedConfig);
    }
}

module.exports.loadConfig = loadConfig