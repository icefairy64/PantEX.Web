const fs = require('fs');
const _ = require('lodash');

const db = require('./db');
const cfg = require('./config');
const crawler = require('./crawler');

let storageRoot;
let taskInterval = 5 * 60 * 1000;
let attachmentsPerTick = 100;

let lock = false;

cfg.onConfigComplete(init, this);
db.onInitComplete(start, this);

async function init(config) {
    storageRoot = config.storage.path;
    if (config.localizer && config.localizer.period) {
        taskInterval = config.localizer.period;
    }
    if (config.localizer && config.localizer.attachmentsPerTick) {
        attachmentsPerTick = config.localizer.attachmentsPerTick;
    }
}

function start() {
    const fn = makeGuardedFn(tick);
    setInterval(fn, taskInterval);
    fn();
}

function makeGuardedFn(afn) {
    return async function () {
        if (lock) {
            console.log('Localizer: exiting because of a present lock');
            return;
        }
        lock = true;
        try {
            await afn();
        }
        finally {
            lock = false;
        }
    };
}

async function tick() {
    const unlocalizedAttachments = [];
    let offset = 0;
    while (unlocalizedAttachments.length < attachmentsPerTick) {
        const posts = await db.posts({
            withNonLocalAttachments: true,
            offset: offset 
        });
        if (posts.length === 0) {
            break;
        }
        offset += posts.length;
        for (let post of posts) {
            for (let attachment of post.attachments) {
                if (attachment.urls.findIndex((url) => url.local === true) < 0) {
                    unlocalizedAttachments.push({ post: post, attachment: attachment });
                }
            }
        }
    }
    console.log('Localizer: processing ' + unlocalizedAttachments.length + ' attachments');
    for (let attachmentCfg of unlocalizedAttachments) {
        const post = attachmentCfg.post;
        const attachment = attachmentCfg.attachment;
        const attachmentIndex = post.attachments.indexOf(attachment);
        console.log('Localizer: fetching ' + JSON.stringify(attachment.urls));
        try {
            let result = await loadAttachment(attachment);
            const url = result.url;
            const filename = url.substring(url.lastIndexOf('/') + 1);
            console.log('Localizer: result.ok = ' + result.content.ok);
            if (result.content.ok === true) {
                const path = await saveAttachment(post, attachmentIndex, filename, result.content);
                db.posts.addAttachmentUrl(post._id, attachmentIndex, { 
                    path: path, 
                    local: true,
                    mimeType: result.content.mimeType
                })
                .then(() => console.log('Localizer: attachment for ' + url + ' saved'));
            }
        }
        catch (e) {
            console.error(e.stack);
        }
    }
}

function createDir(path) {
    return new Promise((resolve, reject) => {
        fs.access(path, (err) => {
            if (err) {
                fs.mkdir(path, (err) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                });
            } else {
                resolve();
            }
        })
    });
}

function writeFile(path, data) {
    return new Promise((resolve, reject) => {
        fs.writeFile(path, data, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        })
    });
}

async function saveAttachment(post, attachmentIndex, originalFilename, content) {
    const targetDir = post._id + '/' + attachmentIndex;
    await createDir(storageRoot + '/' + post._id);
    await createDir(storageRoot + '/' + targetDir);
    if (content.buffer != null) {
        await writeFile(storageRoot + '/' + targetDir + '/' + originalFilename, content.buffer);
    } else if (content.stream != null) {
        const ws = fs.createWriteStream(storageRoot + '/' + targetDir + '/' + originalFilename);
        content.stream.pipe(ws);
    }
    return targetDir + '/' + originalFilename;
}

// FIXME: move to common module
async function loadAttachment(attachment) {
    const originalUrl = attachment.urls.find((url) => _.isString(url) || !url.local);
    if (attachment.tags.includes('lazy')) {
        let config = await require('./refiller/' + attachment.lazyResolveModule)(originalUrl);
        if (_.isString(config)) {
            config = {
                url: config
            }
        }
        const buffer = await crawler.fetchBuffer(config.url);
        return {
            attachment: attachment,
            url: config.url,
            newUrl: config.url,
            content: buffer
        };
    } else {
        const buffer = await crawler.fetchBuffer(originalUrl);
        return {
            attachment: attachment,
            url: originalUrl,
            content: buffer
        };
    }
}

module.exports.saveAttachment = async function (post, attachment, originalUrl, buffer, mimeType) {
    console.log('Localizer: manually saving attachment for ' + originalUrl);
    const attachmentIndex = post.attachments.indexOf(attachment);
    const filename = originalUrl.substring(originalUrl.lastIndexOf('/') + 1);
    const path = await saveAttachment(post, attachmentIndex, filename, { buffer: buffer });
    db.posts.addAttachmentUrl(post._id, attachmentIndex, { 
        path: path, 
        local: true,
        mimeType: mimeType
    })
    .then(() => console.log('Localizer: attachment for ' + originalUrl + ' saved'));
}